package website.web.dto;

import java.util.List;

/**
 * @author Erik Fischer
 */
public class UserDto {

    private String username;
    private List<AnswerDto> answers;
    private List<QuestionDto> questions;
    private String password;

    public UserDto() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
