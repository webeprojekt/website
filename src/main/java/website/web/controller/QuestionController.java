package website.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import website.application.exception.NotFoundException;
import website.persistence.entities.Question;
import website.persistence.entities.User;
import website.service.MyQuestionService;
import website.service.MyUserDetailsService;
import website.web.dto.AnswerDto;
import website.web.dto.QuestionDto;

import java.security.Principal;
import java.util.List;

/**
 * Created by Albert on 6/27/2017.
 */
@RestController
@RequestMapping(value = "/questions")
public class QuestionController {
    private final MyQuestionService questionService;

    @Autowired
    public QuestionController(MyQuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("")
    public List<QuestionDto> getAllQuestions(@RequestParam(required = false) String status) {
        return questionService.getAllQuestions(status);
    }

    @GetMapping("/answeredByMe")
    public List<QuestionDto> getMyQuestions(Principal principal) {
        String userName = principal.getName();
        if (userName != null) {
            return questionService.getQuestionsAnsweredByMe(userName);
        }
        throw new NotFoundException("User", "");
    }

    @GetMapping("/{id:[1-9]+}")
    public QuestionDto getQuestion(@PathVariable("id") Long id) {
        return questionService.getQuestion(id);
    }

    @GetMapping("/{id:[1-9]+}/answers")
    public List<AnswerDto> getAnswersOfQuestion(@PathVariable("id") Long id) {
        return questionService.getAnswersOfQuestion(id);
    }

    @PostMapping("/create")
    public QuestionDto createQuestion(@RequestBody Question question,
                                      Principal principal) {
        String userName = principal.getName();
        if (userName != null)
            return questionService.createQuestion(question, userName);
        throw new NotFoundException("User", "");
    }

    @DeleteMapping(path = "/{id:[1-9]+}/delete")
    public boolean deleteQuestion(@PathVariable("id") Long id,
                                  Principal principal) {
        String userName = principal.getName();
        if (userName != null) {
            return questionService.deleteQuestion(id, userName);
        }
        throw new NotFoundException("User", "");
    }

}
