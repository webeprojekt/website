package website.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import website.application.exception.NotFoundException;
import website.persistence.entities.Answer;
import website.persistence.entities.Question;
import website.persistence.entities.User;
import website.service.MyAnswerService;
import website.service.MyQuestionService;
import website.service.MyUserDetailsService;
import website.web.dto.AnswerDto;
import website.web.dto.QuestionDto;

import java.security.Principal;
import java.util.List;

/**
 * Created by Albert on 6/27/2017.
 */
@RestController
@RequestMapping(value = "/answers")
public class AnswerController {
    private final MyAnswerService answerService;

    @Autowired
    public AnswerController(MyAnswerService answerService) {
        this.answerService = answerService;
    }

    @PostMapping("/create")
    public AnswerDto createAnswer(@RequestParam(value = "content") String content,
                                  @RequestParam(value = "question") Long questionId,
                                  Principal principal) {
        String userName = principal.getName();
        if (userName != null)
            return answerService.addAnswer(content, questionId, userName);
        throw new NotFoundException("User", "");
    }

    @DeleteMapping(path = "/{id:[1-9]+}/delete")
    public boolean deleteAnswer(@PathVariable(value = "id") Long id,
                                  Principal principal) {
        String userName = principal.getName();
        if (userName != null) {
            return answerService.deleteAnswer(id, userName);
        }
        throw new NotFoundException("User", "");
    }

    @PostMapping(path = "/{id:[1-9]+}/accept")
    public boolean acceptAnswer(@PathVariable(value = "id") Long id,
                                  Principal principal) {
        String userName = principal.getName();
        if(userName != null){
            return answerService.acceptAnswer(id, userName);
        }
        throw new NotFoundException("User", "");
    }

}
