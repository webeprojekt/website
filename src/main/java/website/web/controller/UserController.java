package website.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import website.application.exception.EntityAlreadyExistsException;
import website.application.exception.NotFoundException;
import website.persistence.entities.User;
import website.service.UserService;
import website.web.dto.UserDto;

import javax.validation.Valid;
import java.security.Principal;

/**
 * @author Erik Fischer
 */
@Controller
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/signup")
    public @ResponseBody Boolean signup(@Valid @RequestBody User user) {
        try {
            String username = user.getUsername();
            user.setUsername(username.trim());
            userService.addUser(user);
        } catch (EntityAlreadyExistsException e) {
            return false;
        }
        return true;
    }

    @GetMapping("/checkLogin")
    public @ResponseBody String checkLogin(Principal principal) {
        if (principal != null)
            return principal.getName();
        return "";
    }
}
