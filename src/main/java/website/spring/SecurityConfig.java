package website.spring;

/**
 * Created by Albert on 6/27/2017.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;
import website.config.CustomAuthenticationEntryPoint;
import website.service.MyUserDetailsService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //    3) Autowiring and beans for custom UserDetailsService
    private final MyUserDetailsService userDetailsService;

    @Autowired
    public SecurityConfig(MyUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authenticationProvider =
                new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/login");
        web.ignoring().antMatchers("/signup");
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationEntryPoint entryPoint = new CustomAuthenticationEntryPoint();
        http.httpBasic().authenticationEntryPoint(entryPoint)
                .and().exceptionHandling().authenticationEntryPoint(entryPoint)
                .and().authorizeRequests()
                .antMatchers(HttpMethod.GET, new String[]{"/**"}).permitAll()
                .antMatchers(HttpMethod.POST, new String[]{"/**"})
                .fullyAuthenticated()
                .antMatchers(HttpMethod.DELETE, new String[]{"/**"})
                .fullyAuthenticated()
                .antMatchers(HttpMethod.PUT, new String[]{"/**"})
                .fullyAuthenticated()
                .antMatchers(HttpMethod.HEAD, new String[]{"/**"})
                .fullyAuthenticated().and()
                .logout().logoutUrl("/logout").logoutSuccessHandler(
                new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))
                .and().csrf()
                .csrfTokenRepository(csrfTokenRepository()).and()
                .addFilterAfter(csrfHeaderFilter(), CsrfFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {

        //        1) Use this for pure in-memory auth
        //        auth.inMemoryAuthentication().withUser("user").password
        // ("pass").roles("USER")
        //                .and().withUser("admin").password("pass").roles
        // ("USER", "ADMIN");
        //
        //        2) Use this for in-memory database auth
        //        auth.jdbcAuthentication().dataSource(dataSource)
        // .withDefaultSchema()
        //                .withUser("user").password("pass").roles("USER")
        //                .and().withUser("admin").password("pass").roles
        // ("USER", "ADMIN");

        // 3) Extended custom UserDetailsService using own Persistence Objects
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(authProvider());
    }

    private Filter csrfHeaderFilter() {
        return new OncePerRequestFilter() {
            @Override
            protected void doFilterInternal(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain filterChain)
                    throws ServletException, IOException {
                CsrfToken csrf = (CsrfToken) request
                        .getAttribute(CsrfToken.class.getName());
                if (csrf != null) {
                    Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
                    String token = csrf.getToken();
                    if (cookie == null || token != null && !token
                            .equals(cookie.getValue())) {
                        cookie = new Cookie("X-CSRF-TOKEN", token);
                        cookie.setPath("/");
                        response.addCookie(cookie);
                    }
                }
                filterChain.doFilter(request, response);
            }
        };
    }

    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository =
                new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-CSRF-TOKEN");
        return repository;
    }
}
