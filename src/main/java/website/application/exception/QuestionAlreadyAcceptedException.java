package website.application.exception;

/**
 * Created by Albert on 6/27/2017.
 */
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class QuestionAlreadyAcceptedException extends RuntimeException {

    public QuestionAlreadyAcceptedException(Long id) {
        super("The Question with id " + id + " already has an accepted answer!");
    }
}
