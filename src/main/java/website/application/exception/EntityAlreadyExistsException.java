package website.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityAlreadyExistsException extends RuntimeException {
    public EntityAlreadyExistsException(String type, String id) {
        super("Resource of type " + type + " with id " + id + " already exists"
                + ".");
    }
}
