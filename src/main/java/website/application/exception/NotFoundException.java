package website.application.exception;

/**
 * Created by Albert on 6/27/2017.
 */
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException(String type, String id) {
        super("Resource of type " + type + " with id " + id + " not found.");
    }

    public NotFoundException(String type, Long id) {
        super("Resource of type " + type + " with id " + id + " not found.");
    }
}
