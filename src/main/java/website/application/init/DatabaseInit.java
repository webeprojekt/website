package website.application.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import website.persistence.entities.Answer;
import website.persistence.entities.Question;
import website.persistence.entities.User;
import website.persistence.repository.AnswerRepository;
import website.persistence.repository.QuestionRepository;
import website.persistence.repository.UserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Albert on 05.07.2017.
 */
@Component
public class DatabaseInit implements CommandLineRunner {

    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DatabaseInit(QuestionRepository questionRepository, AnswerRepository answerRepository, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... strings) throws Exception {
        boolean initializeDatabase = true;
        for (String arg : strings) {
            if ("-noDBinit".equalsIgnoreCase(arg)) {
                initializeDatabase = false;
            }
        }
        if (initializeDatabase) {
            // Initializes the data of our model
            User banana = new User("Banana", passwordEncoder.encode("banana"));
            banana = userRepository.save(banana);
            User bananaBert = new User("BananaBert", passwordEncoder.encode("bananabert"));
            bananaBert = userRepository.save(bananaBert);
            User hansBanana = new User("HansBanana", passwordEncoder.encode("hansbanana"));
            hansBanana = userRepository.save(hansBanana);
            User bandanaBanana = new User("BandanaBanana", passwordEncoder.encode("bandanabanana"));
            bandanaBanana = userRepository.save(bandanaBanana);
            User bananafan = new User("Bananafan", passwordEncoder.encode("Bananenschale"));
            bananafan = userRepository.save(bananafan);
            User bananaSplit = new User("BananaSplit", passwordEncoder.encode("bananasplit"));
            bananaSplit = userRepository.save(bananaSplit);
//        User user1 = userRepository.findOne("user1");

            Question question1 = new Question("Sind Bananen wirklich gelb?", "Hallo, meine lieben " +
                    "Bananenfreunde sind Bananen wirklich gelb oder doch eher golden? Danke für eure Antworten. Bananige " +
                    "Grüße euer BananaBert", bananaBert);
            question1 = questionRepository.save(question1);

            Answer answer11 = new Answer("Ich bin mir nicht sicher", question1);
            answer11.setUser(bananafan);
            answer11.setCreationDate(1497259344);
            answer11 = answerRepository.save(answer11);
            Answer answer12 = new Answer("Die sind gelb wie die Sonne!", question1);
            //answer12.setAccepted(true);
            answer12.setUser(banana);
            answer12.setCreationDate(1497259581);
            answer11.setCreationDate(System.currentTimeMillis() / 1000L);
            answer12 = answerRepository.save(answer12);
            question1.setStatus(Question.STATUS_UNSOLVED);
            question1.setCreationDate(1499843556);
            question1.addAnswer(answer11);
            question1.addAnswer(answer12);
            question1 = questionRepository.save(question1);

            Question question2 = new Question("Warum ist eine Banane krumm? ", "Warum sind Bananen krumm " +
                    "und warum benutzen sie viele Menschen eigentlich als Pistolen?", bananafan);
            question2.setCreationDate(System.currentTimeMillis() / 1000L);
            question2 = questionRepository.save(question2);

            Question questionSolved = new Question("Warum rutscht man immer auf Bananenschalen aus?", "Wieso passiert mir das ständig?", bandanaBanana);
            questionSolved.setCreationDate(System.currentTimeMillis() / 1000L);
            questionSolved = questionRepository.save(questionSolved);
            Answer answerSolution = new Answer("Also mir passiert sowas nie!", questionSolved);
            answerSolution.setUser(bananaBert);
            answerSolution.setCreationDate(System.currentTimeMillis() / 1000L);
            answerSolution.setAccepted(true);
            answerSolution = answerRepository.save(answerSolution);
            Answer answerSolution2 = new Answer("Erklär doch mal genauer wie dir das immer passiert!", questionSolved);
            answerSolution2.setUser(bananaSplit);
            answerSolution2.setCreationDate(System.currentTimeMillis() / 1000L);
            answerSolution2 = answerRepository.save(answerSolution2);
            Answer answerSolution3 = new Answer("ich verstehe die Frage nicht! ", questionSolved);
            answerSolution3.setUser(banana);
            answerSolution3.setCreationDate(1497259344);
            answerSolution3 = answerRepository.save(answerSolution3);
            questionSolved.setStatus(Question.STATUS_SOLVED);
            questionSolved = questionRepository.save(questionSolved);
        }
    }
}
