package website;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Albert on 6/27/2017.
 */
@SpringBootApplication
public class QASystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(QASystemApplication.class, args);
    }
}
