package website.persistence.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import website.persistence.entities.Question;
import website.persistence.entities.User;

import java.util.List;

/**
 * @author Erik Fischer
 */
@Repository
public interface QuestionRepository extends CrudRepository<Question, Long> {
    @Query("SELECT q FROM Question q WHERE q.status = ?1")
    Iterable<Question> findByStatus(String status);
}
