package website.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import website.persistence.entities.Answer;
import website.persistence.entities.Question;
import website.persistence.entities.User;

import java.util.List;

/**
 * @author Erik Fischer
 */
@Repository
public interface AnswerRepository extends CrudRepository<Answer, Long> {

    Iterable<Answer> findByUser(User user);
}
