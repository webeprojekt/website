package website.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import website.persistence.entities.User;

/**
 * @author Erik Fischer
 */
@Repository
public interface UserRepository extends CrudRepository<User, String> {
}
