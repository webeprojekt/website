package website.persistence.entities;

import org.hibernate.annotations.Type;
import website.web.dto.QuestionDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Erik Fischer
 */
@Entity
public class Question {
    public static final String STATUS_UNANSWERED = "unbeantwortet";
    public static final String STATUS_UNSOLVED = "ungeloest";
    public static final String STATUS_SOLVED = "geloest";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;
    private String content;
    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL, orphanRemoval=true, mappedBy = "question")
    private List<Answer> answers;
    @ManyToOne
    private User user;
    private long creationDate;
    private String status;

    public Question() {
    }

    public Question(String title, String content, User user) {
        this.title = title;
        this.content = content;
        this.user = user;
        this.status = STATUS_UNANSWERED;
        this.answers = new ArrayList<>();
    }

    public void addAnswer(Answer answer){
        this.answers.add(answer);
    }

    public void deleteAnswer(Answer answer) { this.answers.remove(answer); }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Answer> getAnswers() {
        return answers;
    }
//
//    public void setAnswers(List<Answer> answers) {
//        this.answers = answers;
//    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public QuestionDto convertToDto() {
        QuestionDto dto = new QuestionDto();
        dto.setId(this.getId());
//        dto.setAnswers(this.getAnswers());
        dto.setContent(this.getContent());
        dto.setCreationDate(this.getCreationDate());
        dto.setTitle(this.getTitle());
        dto.setUser(this.getUser().getUsername());

        return dto;
    }
}
