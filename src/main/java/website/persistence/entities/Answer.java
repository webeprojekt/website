package website.persistence.entities;

import org.hibernate.annotations.Type;
import website.web.dto.AnswerDto;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Erik Fischer
 */
@Entity
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    private Question question;
    @ManyToOne
    private User user;
    private boolean isAccepted;
    private long creationDate;
    private String content;

    public Answer() {}

    public Answer(String content, Question question) {
        this.content = content;
        this.question = question;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AnswerDto convertToDto() {
        AnswerDto dto = new AnswerDto();
        dto.setAccepted(this.isAccepted());
        dto.setContent(this.getContent());
        dto.setCreationDate(this.getCreationDate());
        dto.setId(this.getId());
        dto.setQuestion(this.getQuestion().getId());
        dto.setUser(this.getUser().getUsername());

        return dto;
    }
}
