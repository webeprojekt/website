package website.persistence.entities;

import org.springframework.beans.factory.annotation.Autowired;
import website.persistence.repository.UserRepository;
import website.web.dto.UserDto;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author Erik Fischer
 */
@Entity
public class User {

    public User(){}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Pattern(regexp = "[a-zA-Z0-9\\._\\-]{4,}")
    @Id
    private String username;

    @OneToMany
    private List<Answer> answers;

    @OneToMany
    private List<Question> questions;

    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserDto convertToDto() {
        UserDto dto = new UserDto();
        dto.setUsername(this.getUsername());
        return dto;
    }
}
