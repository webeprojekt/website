package website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import website.application.exception.EntityAlreadyExistsException;
import website.application.exception.NotFoundException;
import website.persistence.entities.User;
import website.persistence.repository.UserRepository;
import website.web.dto.UserDto;

/**
 * @author Erik Fischer
 */
@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto addUser(User user){
        if(userRepository.findOne(user.getUsername()) != null){
            throw new EntityAlreadyExistsException("User", user.getUsername());
        }
        String unsecuredPassword = user.getPassword();
        String encryptedPassword = passwordEncoder.encode(unsecuredPassword);
        user.setPassword(encryptedPassword);
        userRepository.save(user);
        return user.convertToDto();
    }
}
