package website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.application.exception.NotFoundException;
import website.application.exception.QuestionAlreadyAcceptedException;
import website.persistence.entities.Answer;
import website.persistence.entities.Question;
import website.persistence.entities.User;
import website.persistence.repository.AnswerRepository;
import website.persistence.repository.QuestionRepository;
import website.persistence.repository.UserRepository;
import website.web.dto.AnswerDto;

import java.security.Principal;
import java.util.Date;

/**
 * Created by Albert on 6/27/2017.
 */
@Service
public class MyAnswerService {
    private final MyQuestionService questionService;
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final UserRepository userRepository;

    @Autowired
    public MyAnswerService(MyQuestionService questionService,
                           QuestionRepository questionRepository,
                           AnswerRepository answerRepository,
                           UserRepository userRepository) {
        this.questionService = questionService;
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
    }

    public AnswerDto addAnswer(String content, Long questionId, String userName) {
        Question question = questionRepository.findOne(questionId);
        if (question == null)
            throw new NotFoundException("Question", questionId);
        User user = userRepository.findOne(userName);
        if(user == null){
            throw new NotFoundException("User", "");
        }
        Answer answer = new Answer();
        answer.setContent(content);
        answer.setCreationDate(System.currentTimeMillis() / 1000L);
        System.out.println(System.currentTimeMillis() / 1000L);
        answer.setQuestion(question);
        answer.setUser(user);
        answer = answerRepository.save(answer);
        question.addAnswer(answer);
        if (!question.getStatus().equals(question.STATUS_SOLVED)) {
            question.setStatus(question.STATUS_UNSOLVED);
        }
        questionRepository.save(question);
        return answer.convertToDto();
    }

    public boolean deleteAnswer(Long id, String userName) {
        Answer answer = answerRepository.findOne(id);
        if (answer == null)
            throw new NotFoundException("Answer", id);
        User user = userRepository.findOne(userName);
        if(user == null){
            throw new NotFoundException("User", userName);
        }
        if(user == answer.getUser()){
            Question question = answer.getQuestion();
            question.deleteAnswer(answer);
            if (question.getAnswers().size() == 0)
                question.setStatus(question.STATUS_UNANSWERED);
            else if (question.getStatus().equals(question.STATUS_SOLVED) && answer.isAccepted()) {
                //additional check if other answer have been accepted
                boolean isAnotherAnswerAccepted = false;
                for (Answer a : question.getAnswers()) {
                    if (a.isAccepted()) {
                        isAnotherAnswerAccepted = true;
                    }
                }
                question.setStatus((isAnotherAnswerAccepted ? question.STATUS_SOLVED : question.STATUS_UNSOLVED));
            }
            questionRepository.save(question);
            return true;
        }
        return false;
    }

    public boolean acceptAnswer(Long id, String userName) {
        Answer answer = answerRepository.findOne(id);
        if (answer == null)
            throw new NotFoundException("Answer", id);
        User user = userRepository.findOne(userName);
        if(user == null){
            throw new NotFoundException("User", "");
        }
        if(user == answer.getUser()){
            Long questionId = answer.getQuestion().getId();
            for (AnswerDto a : questionService.getAnswersOfQuestion(questionId)) {
//                if (a.isAccepted())
//                    throw new QuestionAlreadyAcceptedException(questionId);
            }
            Question question = answer.getQuestion();
            question.setStatus(question.STATUS_SOLVED);
            questionRepository.save(question);
            answer.setAccepted(true);
            answerRepository.save(answer);
            return true;
        }
        return false;
    }
}
