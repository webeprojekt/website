package website.service;

import website.application.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.persistence.entities.Answer;
import website.persistence.entities.Question;
import website.persistence.entities.User;
import website.persistence.repository.AnswerRepository;
import website.persistence.repository.QuestionRepository;
import website.persistence.repository.UserRepository;
import website.web.dto.AnswerDto;
import website.web.dto.QuestionDto;

import java.util.*;

/**
 * Created by Albert on 6/27/2017.
 */
@Service
public class MyQuestionService {
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;
    private final UserRepository userRepository;

    @Autowired
    public MyQuestionService(QuestionRepository questionRepository,
                             AnswerRepository answerRepository,
                             UserRepository userRepository) {
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
    }

    public QuestionDto getQuestion(Long id) {
        Question question = questionRepository.findOne(id);
        if (question == null)
            throw new NotFoundException("Question", id);

        return question.convertToDto();
    }

    public List<AnswerDto> getAnswersOfQuestion(Long id) {
        Question question = questionRepository.findOne(id);
        if (question == null)
            throw new NotFoundException("Question", id);

        List<AnswerDto> answers = new LinkedList<>();
        for (Answer answer : answerRepository.findAll()) {
            if (answer.getQuestion().getId().equals(id)) {
                answers.add(answer.convertToDto());
            }
        }
        return answers;
    }

    public QuestionDto createQuestion(Question question, String userName){
        User user = userRepository.findOne(userName);
        if(user == null){
            throw new NotFoundException("User", userName);
        }
        question.setUser(user);
        question.setStatus(question.STATUS_UNANSWERED);
        question.setCreationDate(System.currentTimeMillis() / 1000L);
        System.out.println(System.currentTimeMillis() / 1000L);
        questionRepository.save(question);
        return question.convertToDto();
    }

    public List<QuestionDto> getAllQuestions(String status) {
        List<QuestionDto> questions = new ArrayList<>();
        if (status != null && !status.isEmpty()) {
            addQuestionsToList(questions, questionRepository.findByStatus(status));
            //handle the case when questions have answers but no answer has yet been accepted
            if (status.equals(Question.STATUS_UNSOLVED)) {
                addQuestionsToList(questions, questionRepository.findByStatus(Question.STATUS_UNANSWERED));
            }
        } else {
            addQuestionsToList(questions, questionRepository.findAll());
        }
        return questions;
    }

    private void addQuestionsToList(List<QuestionDto> resultList, Iterable<Question> newQuestions) {
        for (Question question : newQuestions) {
            resultList.add(question.convertToDto());
        }
    }

    public List<QuestionDto> getQuestionsAnsweredByMe(String userName) {
        Set<Long> questionsAnsweredByMeIds = new HashSet<>();
        User user = userRepository.findOne(userName);
        for (Answer answer : answerRepository.findByUser(user)) {
            Long questionId = answer.getQuestion().getId();
            questionsAnsweredByMeIds.add(questionId);
//            QuestionDto questionDto = answer.getQuestion().convertToDto();
//            if (!questions.contains(questionDto))
//                questions.add(questionDto);
        }

        List<QuestionDto> questions = new ArrayList<>();
        for (Question question : questionRepository.findAll(questionsAnsweredByMeIds)) {
            questions.add(question.convertToDto());
        }
        return questions;
    }

    public boolean deleteQuestion(Long id, String userName) {
        Question question = questionRepository.findOne(id);
        if (question == null)
            throw new NotFoundException("Question", id);
        if(question.getUser() == userRepository.findOne(userName)){
            questionRepository.delete(question);
            return true;
        }
        return false;
    }
}
