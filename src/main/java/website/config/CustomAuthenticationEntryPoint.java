package website.config;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Erik Fischer
 */
@Component
public class CustomAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

    public CustomAuthenticationEntryPoint() {
        this.setRealmName("Banana QA Forum");
    }

    @Override
    public void commence(javax.servlet.http.HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException)
            throws IOException, ServletException {
        response.addHeader("WWW-Authenticate", "");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }
}
