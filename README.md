# README #

Zum Erzeugen der jar-Datei in das Repository-Verzeichnis navigieren und 

    mvn package
ausf�hren. 
Mit

    java -jar target/gs-rest-service-0.1.0.jar
l�sst sich der Server starten.
Die Initialisierung der Datenbank l�sst sich mit dem Run-parameter

    -nodbinit
deaktivieren.
Dies wird �ber den Aufruf

    java -jar target/gs-rest-service-0.1.0.jar -nodbinit
realisiert.

Der Server startet auf Port 8080. 
Der Index ist unter 

    http://localhost:8080/index.html
erreichbar.

Teamname:

    Banana Group
Teammitglieder:

    Micha G�nther: 75892
    Michael Sonnleitner: 76109
    Erik Fischer: 78350
    Albert Kiener: 71352
    